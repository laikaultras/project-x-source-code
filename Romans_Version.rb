# Roman's Version Mini_Projekt


require 'Bio'
require 'tempfile'
require 'fileutils'



#puts "Please enter path to input"
file_path_input = ARGV[0]
# file_path_output = ARGV
FileUtils.remove_dir("C:\\Users\\Roman Schauerte\\Desktop\\Mini_Projekt\\output_projekt_all\\") if File.directory?("C:\\Users\\Roman Schauerte\\Desktop\\Mini_Projekt\\output_projekt_all\\")
Dir.mkdir("C:\\Users\\Roman Schauerte\\Desktop\\Mini_Projekt\\output_projekt_all\\") if !File.directory?("C:\\Users\\Roman Schauerte\\Desktop\\Mini_Projekt\\output_projekt_all\\")
sequences1 = {}

#open sequence data
#detect sequence ID 
#detect sequence itself
Bio::FastaFormat.open(file_path_input, "r").each do |line|
  id = line.definition
  sequence = line.seq
  sequences1[id] = sequence
end
#sequences1= sequences1.first(10).to_h
sequences2 = sequences1

sequences1.each do |key1, sequence1|
  sequences2.delete(key1)
  sequences2.each do |key2, sequence2|
  # Hier wird in der ID Zeile die ID gesplittet am "|"
    species1 = key1.split("|")
    species2 = key2.split("|")
    if key1 != key2
    # Hier ist die Outputdatei. Muss nur die Outputdatei verändert werden 
      File.open("C:\\Users\\Roman Schauerte\\Desktop\\Mini_Projekt\\output_projekt_all\\" + "#{species1[3] + "-" + species2[3]}", "w") do |file|
        file.puts sequence1.to_fasta(key1, 80)
        file.puts sequence2.to_fasta(key2, 80)
      end
    end
  end
end



# "C:\\Users\\Roman Schauerte\\Desktop\\Mini_Projekt\\output_projekt_all\\