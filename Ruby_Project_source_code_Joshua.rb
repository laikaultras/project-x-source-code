#Code + FATSA file + Ergebnissordner
require 'bio'
require 'fileutils'
require 'csv'
require 'matrix'
#puts "Please enter path to input"
fasta_path = ARGV[0]
path = ARGV[1]
sequences1 = {}
=begin
FileUtils.remove_dir("#{path}/Pairings") if File.directory?("#{path}/Pairings")                            #löscht den Ordner Pairings falls er bereits existiert
Dir.mkdir("#{path}/Pairings") if !File.directory?("#{path}/Pairings")                                      #erstellt den Ordner Pairings
=end
#open sequence data

Bio::FastaFormat.open(fasta_path).each do |entry|                                                          #öffnet Fasta Datei
  id = entry.definition
  sequence = entry.seq
  sequences1[id] = sequence                                                                                #FASTA Datei ist jetzt in einem Hash
end

#Test für weniger alignements
#sequences1 = sequences1.first(0).to_h
sequences2 = sequences1                                                                                    #2. Hash wird definiert mit den Werten des ersten um im zweiten Hash dateien permanent zu verändern

sequences1.each do |key1, sequence1|
  sequences2.delete(key1)                                                                                  #Sequenz die benutzt wurde wird gelöscht
  sequences2.each do |key2, sequence2|
    id1 = key1.split("|")[0]
    id2 = key2.split("|")[0]
    File.open("#{path}/Pairings/" + id1 + "-" + id2 + ".fasta", "w") do |file|           #wenn Namen nicht gleich sind sollen die 2 Sequenzen in die Datei geschrieben werden und die Namen als Dateiname benutzt werden
      file.puts sequence1.to_fasta(key1, 80)                                                             #Dateien werden im FASTA Format geschrieben
      file.puts sequence2.to_fasta(key2, 80)
    end
  end
end
=begin
FileUtils.remove_dir("#{path}/Alignments") if File.directory?("#{path}/Alignments")                        #löscht den Ordner Alignments falls er bereits existiert
Dir.mkdir("#{path}/Alignments") if !File.directory?("#{path}/Alignments")                                  #erstellt den Ordner Alignments


#Dateipfad Ergebnisordner
Dir.foreach("#{path}/Pairings/") do |filename|
  next if filename == '.' || filename == '..' || filename == ".DS_Store"
  puts "working on #{filename}"
  `mafft --maxiterate 1000 "#{path}/Pairings/#{filename}" > "#{path}/Alignments/#{filename.gsub(".fasta", "_alignment.fasta")}"`
end
=end
matrix = {}
family = {}
genus = {}
species = {}
all = [] 

Dir.foreach("#{path}/Alignments/") do |filename|                                                             #Ordner Pairings wird geöffnet(in Alignment war noch nix)
  alignment = []
  names = []
                
  next if filename == '.' || filename == '..' || filename == ".DS_Store" 
  puts "working on #{filename}"                                                                            #an welcher Datei wird gearbeitet
                                                                               
  Bio::FastaFormat.open("#{path}/Alignments/#{filename}", "r").each do |entry|                              #jede datei wird im FASTA format geöffnet     
    alignment << entry.seq  
    names << entry.definition  
    # names = names.uniq      
  end
  
  matches_count = 0.0
  mismatch_count = 0.0
  (0...alignment[0].length).each do |index|                                                             #von 0 bis Ende der Sequenz soll etwas getan werden
    if alignment[0][index] == alignment[1][index]
      matches_count += 1
    elsif alignment[0][index] == "-" || alignment[1][index] == "-"
      next
    else
      mismatch_count += 1
    end
  end
    
    
  p_distance = mismatch_count / (matches_count + mismatch_count)                                                      #ausrechnen der p-Distanzen
  p_distance = p_distance.round(4)                                                                      #runden bis auf die dritte Nachkommastelle
  
  all << p_distance
  
  if !matrix.has_key?(names[0])                                                                         #überprüfen ob der key in matrix schon vorhanden ist
    matrix[names[0]] = {}                                                                               #wenn der key noch nicht vorhanden ist soll dieser erstellt werden
  end
  matrix[names[0]][names[1]] = p_distance                                                             #p-Distanz wird dem key zugeordnet
  

  matrix = matrix.sort_by{|names, inner_hash| inner_hash.length}.to_h                                   #matrix wird nach den längen der values sortiert

  
  fam_name = names[0].split("|")[4]
  sec_fam_name = names[1].split("|")[4]

  if fam_name == sec_fam_name

    if !family.has_key?(fam_name) 
      family[fam_name] = []
    end
    family[fam_name] << p_distance
  end


  spe_name = names[0].split("|")[3]
  sec_spe_name = names[1].split("|")[3]

  if spe_name == sec_spe_name

    if !species.has_key?(spe_name) 
      species[spe_name] = []
    end
    species[spe_name] << p_distance
  end


  gen_name = names[0].split("|")[3]
  gen_name = gen_name.split("_")[0]
  sec_gen_name = names[1].split("|")[3]
  sec_gen_name = sec_gen_name.split("_")[0]

  if gen_name == sec_gen_name

    if !genus.has_key?(gen_name) 
      genus[gen_name] = []
    end
    genus[gen_name] << p_distance
  end
end

CSV.open("/Users/joshi/Desktop/matrix.csv", "wb", headers: ["", matrix.keys].flatten, write_headers: true) do |csv|
  matrix.each do |id, inner_hash|
    csv << [id, inner_hash.values].flatten
  end
end


family.each do |fam_name, p_distance|
  family_sum = p_distance.sum / p_distance.length.to_f
  p "The mean p distance for the families is #{family_sum.round(4)} for #{fam_name}"
  CSV.open("/Users/joshi/Desktop/family.csv", "wb", headers: ["", family.keys].flatten, write_headers: true) do |csv|
    #family.each do |definition, |
      csv << [fam_name, family_sum]
    #end
  end
end 






genus.each do |gen_name, p_distance|
  genus_sum = p_distance.sum / p_distance.length.to_f
  p "The mean p distance for the genus is #{genus_sum.round(4)} for #{gen_name}"
end 

CSV.open("/Users/joshi/Desktop/genus.csv", "wb", headers: ["", genus.keys].flatten, write_headers: true) do |csv|
  genus.each do |definition, array|
    csv << [definition, array].flatten
  end
end


species.each do |spe_name, p_distance|
  species_sum = p_distance.sum / p_distance.length.to_f
  p "The mean p distance for the species is #{species_sum.round(4)} for #{spe_name}"
end 

CSV.open("/Users/joshi/Desktop/species.csv", "wb", headers: ["", species.keys].flatten, write_headers: true) do |csv|
  species.each do |definition, array|
    csv << [definition, array].flatten
  end
end

p "The mean p distance for all is #{all.sum / all.length}"


