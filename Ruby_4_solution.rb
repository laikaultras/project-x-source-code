# Task 1
puts "What year?"
year = gets.chomp.to_i

if year%4 == 0 # Year is divisible by 4
    if year%100 != 0 # Year is not a century
      puts "The year #{year.to_s} is a leap year!"
    else
      if year%400 == 0
          print "The year #{year.to_s} is a leap year!"
      else
          print "The year #{year.to_s} is not a leap year!"
      end
    end
else
  puts "The year #{year.to_s} is not a leap year!"
end

# Task 2
puts "Tell me a sentence."
sentence = gets.chomp
words = sentence.split
sum = 0

words.each do |word|
  sum += word.length
end
		
puts "The average word length is #{(sum.to_f/words.length).round(2)}."

# Task 3
puts "Tell me a sentence."
sentence = gets.chomp
words = sentence.split
acronym = []

words.each do |word|
  acronym << word[0].upcase
end
		
puts "Acronym: #{acronym.join}"

# Task 4
puts "Tell me a sentence."
sentence = gets.chomp.downcase
words = sentence.split		
p words.sort

# Task 5
puts "Input a DNA sequence."
sequence = gets.chomp.upcase
complement = ""

bases = {'A' => 'T', 
         'T' => 'A', 
         'C' => 'G', 
         'G' => 'C' }

(0...sequence.length).each do |index|
  current_base = sequence[index]
  complement <<  bases[current_base]
end

puts "Reverse complement: #{complement.reverse}"

# Task 6
puts "Input a DNA sequence."
sequence = gets.chomp.upcase

sequence_length = sequence.length.to_f
rel_frequency = {}

rel_frequency[:a] = (sequence.count('A')/sequence_length).round(4)
rel_frequency[:t] = (sequence.count('T')/sequence_length).round(4)
rel_frequency[:g] = (sequence.count('G')/sequence_length).round(4)
rel_frequency[:c] = (sequence.count('C')/sequence_length).round(4)

p sequence_length
p rel_frequency

# Task 7
puts "Input a DNA sequence."
sequence = gets.chomp.upcase

sequence_length = sequence.length
frequency_a = sequence.count('A')
frequency_t = sequence.count('T')
frequency_g = sequence.count('G')
frequency_c = sequence.count('C')

melting_temperature = 0

if sequence_length < 14
  melting_temperature = 4 * (frequency_g + frequency_c) + 2 * (frequency_a + frequency_t)
else
  melting_temperature = (64.9 + 41 * ((frequency_g + frequency_c)-16.4)/sequence_length.to_f).round(2)
end

puts sequence_length
puts melting_temperature

# Task 8
puts "Input a DNA sequence."
sequence = gets.chomp.upcase

sequence_length = sequence.length
frequency_a = sequence.count('A')
frequency_t = sequence.count('T')
frequency_g = sequence.count('G')
frequency_c = sequence.count('C')

if (frequency_a + frequency_t + frequency_g + frequency_c) == sequence_length
  melting_temperature = 0

  if sequence_length < 14
    melting_temperature = 4 * (frequency_g + frequency_c) + 2 * (frequency_a + frequency_t)
  else
    melting_temperature = (64.9 + 41 * ((frequency_g + frequency_c)-16.4)/sequence_length.to_f).round(2)
  end

  puts sequence_length, melting_temperature
else
  puts "Warning: Not all characters are valid DNA bases."
end

# Task 9
# a) It calculates the cube of a number.
# b
def cube(x)
  x*x*x
end

y = 3
puts cube(y)

# Task 10
# a
def sum_array(array)
  sum = 0
  array.each { |number| sum += number }
  sum
end

array = [1, 2, 3, 4, 5]
puts sum_array(array)

# b
def array_mean(array)
  sum_array(array)/array.length
end

def array_standard_variation(array)
  sum = 0
  array.each { |number| sum += (number - array_mean(array))**2 }
  sum / array.length
end

def array_standard_deviation(array)
  Math.sqrt(array_standard_variation(array))
end

puts array_mean(array), array_standard_variation(array), array_standard_deviation(array)

# Task 11
phone_book = {
  sarah: 1234,
  lara: 8976,
  mom: 4376,
  dad: 46381,
}

# a
puts phone_book.length

# b
puts phone_book[:sarah]
puts phone_book.has_value?(1234)
# Looking up a key is more efficient than looking up a value (imagine searching for a German word in an English -> German dictionary without any knowledge of the English word...)

# c
phone_book[:susi] = 7634
p phone_book

# d
phone_book[:sarah] = 23456

# e
phone_book[:me] = phone_book.delete :sarah # phone_book.delete will delete an element and return the deleted value

# f
p phone_book.keys.sort

# Task 12
dictionaries = { 
  german: {
    'hello' => 'Hallo',
    'day' => 'Tag',
    'night' => 'Nacht',
    'dog' => 'Hund',
    'cat' => 'Katze'
  },
  swedish: {
    'hello' => 'hej',
    'day' => 'dag',
    'night' => 'natt',
    'dog' => 'hund',
    'cat' => 'katt'
  }
}

p dictionaries

# Example for looping over user input
while user_input = gets.chomp
  if user_input == "exit"
    break
  else
    puts "Hello, #{user_input}!"    
  end
end
