# Roman's Version Mini_Projekt


require 'Bio'
require 'tempfile'




#puts "Please enter path to input"
file_path_input = ARGV
file_path_output = ARGV
sequences1 = {}

#open sequence data
Bio::FastaFormat.open(file_path_input[0], "r").each do |line|
  id = line.definition
  sequence = line.seq
  sequences1[id] = sequence
end

sequences2 = sequences1

sequences1.each do |key1, sequence1|
  sequences2.delete(key1)
  sequences2.each do |key2, sequence2|
    species1 = key1.split("|")
    species2 = key2.split("|")
    if key1 != key2
      File.open(file_path_output[1] + "#{species1[3] + "-" + species2[3]}", "w") do |file|
        file.puts sequence1.to_fasta(key1, 80)
        file.puts sequence2.to_fasta(key2, 80)
      end
    end
  end
end



