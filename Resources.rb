http://bioruby.org/rdoc/Bio/FastaFormat.html

f.entry_id #=> "gi|398365175"
f.first_name #=> "gi|398365175|ref|NP_009718.3|"
f.definition #=> "gi|398365175|ref|NP_009718.3| Cdc28p [Saccharomyces cerevisiae S288c]"
f.identifiers #=> Bio::FastaDefline instance
f.accession #=> "NP_009718"
f.accessions #=> ["NP_009718"]
f.acc_version #=> "NP_009718.3"
f.comment #=> nil


#Dateipfad Ergebnisordner
Pairings_dir = ARGV[1]
Dir.foreach(Pairings_dir) do |filename|
  next if filename == '.' || filename == '..'
  puts "working on #{filename}"
  file = File.open("#{pairings_dir}/#{filename}", "r")
  mafft --maxiterate 1000 
  file.close
end