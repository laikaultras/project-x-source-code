#Code + FATSA file + Ergebnissordner
require 'bio'
require 'fileutils'
require 'csv'
require 'matrix'
#puts "Please enter path to input"
fasta_path = ARGV[0]
path = ARGV[1]
sequences1 = {}


FileUtils.remove_dir("#{path}/Pairings") if File.directory?("#{path}/Pairings")                            #löscht den Ordner Pairings falls er bereits existiert
Dir.mkdir("#{path}/Pairings") if !File.directory?("#{path}/Pairings")                                      #erstellt den Ordner Pairings

#open sequence data

Bio::FastaFormat.open(fasta_path).each do |entry|                                                          #öffnet Fasta Datei
  id = entry.definition
  sequence = entry.seq
  sequences1[id] = sequence                                                                                #FASTA Datei ist jetzt in einem Hash
end


sequences1 = sequences1.first(10).to_h
sequences2 = sequences1                                                                                    #2. Hash wird definiert mit den Werten des ersten um im zweiten Hash dateien permanent zu verändern

sequences1.each do |key1, sequence1|
  sequences2.delete(key1)                                                                                  #Sequenz die benutzt wurde wird gelöscht
  sequences2.each do |key2, sequence2|
    species1 = key1.split("|")
    species2 = key2.split("|")
    if key1 != key2
      File.open("#{path}/Pairings/" + species1[0] + "-" + species2[0] + ".fasta", "w") do |file|           #wenn Namen nicht gleich sind sollen die 2 Sequenzen in die Datei geschrieben werden und die Namen als Dateiname benutzt werden
        file.puts sequence1.to_fasta(key1, 80)                                                             #Dateien werden im FASTA Format geschrieben
        file.puts sequence2.to_fasta(key2, 80)
      end
    end
  end
end

FileUtils.remove_dir("#{path}/Alignments") if File.directory?("#{path}/Alignments")                       #löscht den Ordner Alignments falls er bereits existiert
Dir.mkdir("#{path}/Alignments") if !File.directory?("#{path}/Alignments")                                 #erstellt den Ordner Alignments


 #Dateipfad Ergebnisordner
 Dir.foreach("#{path}/Pairings/") do |filename|
   next if filename == '.' || filename == '..' || filename == ".DS_Store"
   puts "working on #{filename}"
   `mafft --maxiterate 1000 "#{path}/Pairings/#{filename}" > "#{path}/Alignments/#{filename.gsub(".fasta", "_alignment.fasta")}"`
 end

entry_id = {}
matrix = {}
Dir.foreach("#{path}/Pairings/") do |filename|                                                               #Ordner Pairings wird geöffnet(in Alignment war noch nix)
  alignment = []
  next if filename == '.' || filename == '..' 
  puts "working on #{filename}"                                                                             #an welcher Datei wird gearbeitet
  Bio::FastaFormat.open("#{path}/Pairings//#{filename}", "r").each do |entry|                             #jede datei wird im FASTA format geöffnet     
    alignment << entry.seq
    entry_id = entry.definition            
  end
  # puts alignment        
  # puts "====="
  matches_count = 0
  matches = ""
  mismatch_count = 0
  mismatches = ""
    (0...alignment[0].length).each do |index|                                                             #von 0 bis Ende der Sequenz soll etwas getan werden
      if alignment[0][index] == alignment[1][index]
        matches_count += 1
      else
        mismatch_count += 1
      end
    end
 
    matches = matches_count.to_f
    mismatches = mismatch_count.to_f
    #p_distance = mismatches / (matches + mismatches)
    #p_distance = p_distance.round(2)
    #p_distance_all = []
    #p_distance_all << p_distance
    

  Bio::FastaFormat.open("#{path}/Pairings//#{filename}", "r").each do |entry|                           #jede datei wird im FASTA format geöffnet     
    id1 = entry.definition
    id1 = id1.split("|")
    id1 = id1[3]
    id2 = entry.definition
    id2 = id2.split("|")
    id2 = id2[3]
    #sequence1 = entry.seq.split("")
    #sequence2 = entry.seq.split("")
 
    datadump = [i][c]
      for i = 0 && i < 9 && i += 1
       for c = 0 && c < 9 && c += 1
            p_distance = mismatches / (matches + mismatches)
          datadump[i][c] = p_distance
        end
      end
    



    #matrix[id1] = {[id2] => p_distance}
    #matrix[id2] = [p_distance] 
    p matrix
  #puts id1
  #puts id2




CSV.open("/Users/joshi/Desktop/file.csv", "wb", headers: ["", matrix.keys].flatten, write_headers: true) do |csv|
  matrix.each do |key, p_distance|
   csv << [key.to_s, p_distance.values].flatten
  end
  end
end
end
end